import boto3

key = "$AWS_ACCESS_KEY_ID"
secret = "$AWS_SECRET_ACCESS_KEY"

bucket_name = 'habi-test'
file_name = 'habi.png'

def lambda_handler(event, context):
    s3 = boto3.client('s3')
    key_name = file_name
    s3.upload_file(file_name, bucket_name, key_name)

    return {
       'status': 'True',
       'statusCode': 200,
       'body': 'Image Uploaded'
      }


print(lambda_handler("", ""))
